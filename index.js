/**
 * setVersion
 * https://bitbucket.org/codeprojectsrepo/setversion.git
 *
 * Copyright (c) 2015 Aaldert van Weelden
 * Licensed under the MIT license.
 */


/**
 * Depedencies
 */
var gulp = require('gulp');
var fs = require('fs');
var prompt = require('prompt');
var pkg = require('./../../package.json');
var bwr = require('./../../bower.json');
var git = require('gulp-git');
var dateUtils = require('date-utils');
var mkdirp = require('mkdirp');

var FILE_ENCODING = 'utf-8';
var DATETIME_FORMAT = 'YYYY-MM-DD HH24:MI:SS';
var EOL = '\n';

module.exports = {
		
		commit : function(){
			
			var schema = {
				    properties: {
				        version: {
				          description : 'version, preferred format is "RELEASE-[version]" or "SNAPSHOT-[version]',
				          message: 'Version is required, preferred format is "RELEASE-[version]" or "SNAPSHOT-[version]"',
				          default : pkg.status==''?pkg.version:pkg.status+'-'+pkg.version,
				          required: true
				        },
				        project: {description : 'application', default : pkg.name,},
				        user: {default : pkg.author},
				        description : {default : pkg.description},
				        changelog : {description : 'add the changelog entries as CSV '}
				      }
				    };

			  prompt.start();
			  prompt.message = "    >>";

			  prompt.get(schema, function (err, input) {
				  
				  if (err) throw err;
				  
				  
				  var version = [];
				  var minus;
				  
				  if(input.version.indexOf("-")!=-1){
					  version = input.version.split("-");
					  minus = '-';
				  }else{
					  version[0] = '';
					  version[1] = input.version;
					  minus = '';
				  }
				  
				  var now = new Date();
				  //write the input to the package file
				  pkg.name = input.project;
				  bwr.name = input.project;
				  pkg.version = version[1];
				  bwr.version = version[1];
				  pkg.author = input.user;
				  bwr.authors = [input.user];
				  pkg.description = input.description;
				  bwr.description = input.description;
				  pkg.timestamp = now.toFormat(DATETIME_FORMAT);
				  bwr.timestamp = pkg.timestamp
				  pkg.status = version[0];
				  bwr.status = version[0];
				  fs.writeFileSync('package.json', JSON.stringify(pkg, null, 4), FILE_ENCODING);
				  console.log(' package.json is updated'); 
				  fs.writeFileSync('bower.json', JSON.stringify(bwr, null, 4), FILE_ENCODING);
				  console.log(' bower.json is updated');
				  
				  mkdirp('./dist', function (err) {
					    if (err) throw (err);
					});
					    
				  var destPath = './dist/changelog.txt';
				  var txt = '';
				  var hr = '--------------------------------------------'+
					       '--------------------------------------------';
					    
				  try{
					   txt = fs.readFileSync(destPath, FILE_ENCODING);
				  }catch(e){
					   txt = '*** '+input.project+' CHANGELOG ***'+EOL;
				  }
					    
				  txt += EOL+EOL+hr+EOL+pkg.timestamp+' changelog for '+input.project+' version '+input.version+' : '+EOL;
				  var changelog = input.changelog+EOL;
				  if(input.changelog.indexOf(',')!=-1){
					  changelog = input.changelog.split(',');
					  for(var i=0;i<changelog.length;i++){
						  txt += '* ' + changelog[i]+EOL;
					  }
				  }else{
					  txt += '* ' + changelog+EOL;
				  }
				  console.log(' Trying to update changelog...');
				  fs.writeFileSync(destPath, txt, FILE_ENCODING);
				  console.log(' '+destPath+' is written');    
				 

				  
				  gulp.src('./*.json').pipe(git.commit('Committing project version '+input.version));
				  
		  
					  var schema2 = {
							    properties: {
							        writeTag: {
								          description : 'write TAG to GIT ? (Y/n)',
								          message: 'Please choose Y(es) or n(o)',
								          default : "n",
								          required: true
								        }
							      }
							    };

						  prompt.start();
						  prompt.message = "    >>>>";

						  prompt.get(schema2, function (err, input) {
							  
							  if (err) throw err;

							  if(input.writeTag =="Y"){
								//write tag to GIT
								  git.tag(pkg.status+minus+pkg.version, 'Tagging '+pkg.name+' to '+pkg.status+minus+pkg.version, function (err) {
									  if (err) throw err;
								  });
								  console.log('Project is tagged to '+pkg.status+minus+pkg.version);
								  
							  }else{
								  console.log('No tag written, perform this manually please to '+pkg.status+minus+pkg.version);
							  }
							  
							  
							  var schema3 = {
									    properties: {
									        push: {
										          description : 'push version change  and tag ['+pkg.status+minus+pkg.version+'] to MASTER remote ? (Y/n)',
										          message: 'Please choose Y(es) or n(o)',
										          default : "n",
										          required: true
										        }
									      }
									    };

								  prompt.start();
								  prompt.message = "    >>>>";

								  prompt.get(schema3, function (err, input) {
									  
									  if (err) throw err;

									  if(input.push =="Y"){
										//push to GIT remote
										 
									      git.push('origin', 'master', {args: " --tags"}, function (err) {
											 if (err) throw err;
										  });
											
										  
										  console.log('Trying to push project version '+pkg.status+minus+pkg.version+' to MASTER remote...');
										  
									  }else{
										  console.log('Nothing is pushed, please perform this step manually for project version'+pkg.status+minus+pkg.version);
									  }
									  
									  
								  });
							  
							  
						  });
		 
				  
				  
			  });
		}
};